'use strict'

const Boom = require('boom');
const ChildProcess = require('child_process');
const PythonShell = require('python-shell');
const Hoek = require('hoek');
const wsdl = 'https://services.chromedata.com/Description/7b?wsdl';
const soap = require('soap');
const args = {
  "accountInfo": {
    "attributes": {
      "number": "265491",
      "secret": "24a63940ed48890b",
      "country": "US",
      "language": "en",
      // "ShowAvailableEquipment": true
    }
  },
  // "showAvailableEquipment": true
};
exports.register = function(server, options, next) {

  server.route([{
      method: 'GET',
      path: '/model-years',
      config: {
        description: 'Get all model years',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/model-years
          */
          soap.createClient(wsdl, function(err, client) {

            client.getModelYears(args, function(err, result) {
              // reply(result.modelYear)
              console.log(err)
              if (result.modelYear) {

                reply(result.modelYear)
              } else {
                reply(result)
              }
            });
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/category-definitions',
      config: {
        description: 'Get all category definitions',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/category-definitions
          */

          soap.createClient(wsdl, function(err, client) {

            client.getCategoryDefinitions(args, function(err, result) {

              if (result.category) {

                reply(result.category)
              } else {
                reply(result)
              }
              // reply(result.category)
            });
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/styles/{model}',
      config: {
        description: 'Get styles based off of model ID',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/styles/30289
          */
          let model = request.params.model
          args.attributes = {
            "modelId": model
          }
          console.log(args)
          soap.createClient(wsdl, function(err, client) {

            client.getStyles(args, function(err, result) {
              delete args.attributes
              if (result.style) {

                reply(result.style)
              } else {
                reply(result)
              }
            });
          });
        }
      }
    },
    {
      method: 'POST',
      path: '/models',
      config: {
        description: 'Get models based off of a year and a division id (see /divisions)',
        handler: function(request, reply) {
          let model = request.payload
          /*
          ---------- Example Payload
          {
          year: 2018,
          divisionId: 8
          }
          */
          args.modelYear = model.year;
          args.divisionId = model.divisionId;
          soap.createClient(wsdl, function(err, client) {

            client.getModels(args, function(err, result) {

              // reply(result.model)
              delete args.modelYear
              delete args.divisionId
              if (result.model) {

                reply(result.model)
              } else {
                reply(result)
              }
            });
          });
        }
      }
    },
    {
      method: 'POST',
      path: '/models-by-subdivision',
      config: {
        description: 'Get models based off of a year and a subdivision id ',
        handler: function(request, reply) {
          let model = request.payload
          /*
          ---------- Example Payload
          {
          year: 2018,
          subdivisionId: 9008
          }
          */
          args.modelYear = model.year;
          args.subdivisionId = model.subdivisionId;
          soap.createClient(wsdl, function(err, client) {

            client.getModels(args, function(err, result) {

              // reply(result.model)
              delete args.modelYear
              delete args.subdivisionId
              if (result.model) {

                reply(result.model)
              } else {
                reply(result)
              }
            });
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/technical-specs',
      config: {
        description: 'Get all the technical specs',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/technical-specs
          */
          soap.createClient(wsdl, function(err, client) {

            client.getTechnicalSpecificationDefinitions(args, function(err, result) {
              console.log(err)
              if (result.definition) {

                reply(result.definition)
              } else {
                reply(result)
              }
            });
          });

        }
      }
    },
    {
      method: 'GET',
      path: '/subdivisions/{year}',
      config: {
        description: 'Get all subdivisions based off of year',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/subdivisions/2018
          */
          let year = request.params.year
          args.attributes = {
            "modelYear": year
          }
          soap.createClient(wsdl, function(err, client) {

            client.getSubdivisions(args, function(err, result) {
              delete args.attributes
              if (result.subdivision) {

                reply(result.subdivision)
              } else {
                reply(result)
              }
              args.attributes = {
                "showAvailableEquipment": true
              }
            });
          });

        }
      }
    },
    {
      method: 'GET',
      path: '/divisions/{year}',
      config: {
        description: 'Get all divisions based off of year',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/divisions/2018
          */
          let year = request.params.year
          args.attributes = {
            "modelYear": year
          }
          soap.createClient(wsdl, function(err, client) {

            client.getDivisions(args, function(err, result) {

              // reply(result.division)
              // console.log(err)
              delete args.attributes
              if (result.division) {

                reply(result.division)
              } else {
                reply(result)
              }
            });
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/describe-vehicle/{vin}',
      config: {
        description: 'Get a vin description. ',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/describe-vehicle/1GCVKREC3JZ128186
          */
          let vin = request.params.vin

          args.vin = vin
          // args.styleId = 384643
          // args.switch = ["ShowAvailableEquipment"]
          // args.OEMOptionCode = ["AAQ", "AL9", "ATG", "AT9", "A28", "BTV", "BWN", "BW5", "B30", "B34", "B35", "CTT", "C49", "C5J", "C68", "DD8", "DG6", "D75",
          //   "E63", "FE9", "GBA", "GU6", "G80", "HH1", "IO5", "KA1", "K05", "K34", "LGZ", "M5T", "NQ6", "NY7", "PPA", "QHE", "R28", "S1K", "T3C", "T3U", "UDD",
          //   "UE1", "UQA", "UTJ", "UVC", "U2K", "VJQ", "VK3", "VT5", "VV4", "W1Y", "ZJJ", "Z71", "Z82", "4Z7", "5GD", "9B7"
          // ]



          soap.createClient(wsdl, function(err, client) {

            client.describeVehicle(args, function(err, result) {

              console.log(err)
              delete args.vin
              delete args.switch
              // delete args.OEMOptionCode
              reply(result)
            });
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/describe-vehicle-options/{vin}',
      config: {
        description: 'Get a vin description and options ',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/describe-vehicle/1GCVKREC3JZ128186
          */
          let vin = request.params.vin

          args.vin = vin
          // args.styleId = 384643
          args.switch = ["ShowAvailableEquipment"]
          // args.OEMOptionCode = ["AAQ", "AL9", "ATG", "AT9", "A28", "BTV", "BWN", "BW5", "B30", "B34", "B35", "CTT", "C49", "C5J", "C68", "DD8", "DG6", "D75",
          //   "E63", "FE9", "GBA", "GU6", "G80", "HH1", "IO5", "KA1", "K05", "K34", "LGZ", "M5T", "NQ6", "NY7", "PPA", "QHE", "R28", "S1K", "T3C", "T3U", "UDD",
          //   "UE1", "UQA", "UTJ", "UVC", "U2K", "VJQ", "VK3", "VT5", "VV4", "W1Y", "ZJJ", "Z71", "Z82", "4Z7", "5GD", "9B7"
          // ]



          soap.createClient(wsdl, function(err, client) {

            client.describeVehicle(args, function(err, result) {

              console.log(err)
              delete args.vin
              delete args.switch
              // delete args.OEMOptionCode
              reply(result)
            });
          });
        }
      }
    },

    {
      method: 'GET',
      path: '/describe-vehicle-show-available/{styleId}',
      config: {
        description: 'Get a chrome style id description and options ',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/describe-vehicle/406768
          */
          let styleId = request.params.styleId
          args.styleId = styleId
          args.switch = ["ShowAvailableEquipment"]
          soap.createClient(wsdl, function(err, client) {
            client.describeVehicle(args, function(err, result) {
              console.log(err)
              delete args.styleId
              delete args.switch
              reply(result)
            });
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/version-info',
      config: {
        description: 'Get the data version information',
        handler: function(request, reply) {
          /*
          example: https://beta.rydellvision.com:8888/chrome/version-info
          */
          soap.createClient(wsdl, function(err, client) {
            client.getVersionInfo(args, function(err, result) {
              console.log(err)
              if (result.definition) {
                reply(result.definition)
              } else {
                reply(result)
              }
            });
          });

        }
      }
    },
  ]);

  next();
};

exports.register.attributes = {
  name: 'chrome-data'
};