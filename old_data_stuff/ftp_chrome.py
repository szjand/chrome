
# # this is ok for downloading a single hardcoded filename
# from ftplib import FTP
# ftp = FTP("ftp.chromedata.com", "u265491", "car491")
# # ftp.login()
# ftp.retrlines('NLST')
# ftp.cwd('/VINMatch_US/')
# ftp.retrlines('NLST')
# filename = 'VINDATA.zip'
# localfile = open('files/' + filename, 'wb')
# ftp.retrbinary('RETR ' + filename, localfile.write, 1024)
# ftp.quit()
# localfile.close()

# from http://motoma.io/using-python-to-leech-files-from-an-ftp-server/


import ftplib

# Connection information
server = 'ftp.chromedata.com'
username = 'u265491'
password = 'car491'

# Directory and matching information
directory_new = '/NVD_Fleet_US_EN/All/'
directory_used = '/VINMatch_US/'
filematch_new = 'nvd*'
filematch_used = '*.zip'

# Establish the connection
ftp = ftplib.FTP(server)
ftp.login(username, password)

# Change to the proper directory
ftp.cwd(directory_new)

# Loop through matching files and download each one individually
# overwrites existing files in \files directory
for filename in ftp.nlst(filematch_new):
    fhandle = open('files/' + filename, 'wb')
    print('Getting ' + filename)
    ftp.retrbinary('RETR ' + filename, fhandle.write)
    fhandle.close()

ftp.cwd(directory_used)

for filename in ftp.nlst(filematch_used):
    fhandle = open('files/' + filename, 'wb')
    # print 'Getting ' + filename
    ftp.retrbinary('RETR ' + filename, fhandle.write)
    fhandle.close()

ftp.quit()
