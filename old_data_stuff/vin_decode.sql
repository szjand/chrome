﻿do
$$
DECLARE _vin text;

begin
_vin := '3GCUKSEC8EG108427';
--_vin = '1G4HP53L4NH458470';
drop table if exists _vin;

create temporary table _vin as
SELECT *
FROM chr.vin_pattern
WHERE substring(vin_pattern,1,1) = substring(_vin,1,1)
AND   substring(vin_pattern,2,1) = substring(_vin,2,1)
AND   substring(vin_pattern,3,1) = substring(_vin,3,1)
AND   substring(vin_pattern,4,1) = substring(_vin,4,1)
AND   substring(vin_pattern,5,1) = substring(_vin,5,1)
AND   substring(vin_pattern,6,1) = substring(_vin,6,1)
AND   substring(vin_pattern,7,1) = substring(_vin,7,1)
AND   substring(vin_pattern,8,1) = substring(_vin,8,1)
AND   substring(vin_pattern,10,1) = substring(_vin,10,1)
AND   ((substring(vin_pattern,11,1) = substring(_vin,11,1)) OR (substring(vin_pattern,11,1) = '*'))
AND   ((substring(vin_pattern,12,1) = substring(_vin,12,1)) OR (substring(vin_pattern,12,1) = '*'))
AND   ((substring(vin_pattern,13,1) = substring(_vin,13,1)) OR (substring(vin_pattern,13,1) = '*'))
AND   ((substring(vin_pattern,14,1) = substring(_vin,14,1)) OR (substring(vin_pattern,14,1) = '*'))
AND   ((substring(vin_pattern,15,1) = substring(_vin,15,1)) OR (substring(vin_pattern,15,1) = '*'))
AND   ((substring(vin_pattern,16,1) = substring(_vin,16,1)) OR (substring(vin_pattern,16,1) = '*'))
AND   ((substring(vin_pattern,17,1) = substring(_vin,17,1)) OR (substring(vin_pattern,17,1) = '*'));

END
$$;

select * from _vin ;


do
$$
declare _vin text;
begin 
_vin := '3GCUKSEC8EG108427';
--_vin = '1G4HP53L4NH458470';
drop table if exists _x;

create temporary table _x as
SELECT *
FROM chr.Vin_Pattern
WHERE LEFT(Vin_Pattern, 8) = LEFT(_vin, 8)
AND _vin LIKE REPLACE(vin_pattern, '*', '_');
end
$$;
select * from _x;

create or replace function chr.decode_vin(_vin citext)
returns setof chr.vin_pattern as
$$
select *
from chr.vin_pattern
where left(vin_pattern, 8) = left(_vin, 8)
  and _vin like replace(vin_pattern, '*', '_');
$$ 
language sql;

select * from chr.decode_vin('3GCUKSEC8EG108427')