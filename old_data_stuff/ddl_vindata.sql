﻿create TABLE if not exists chr.year_make_model_style(
    chrome_style_id integer primary key,
    country citext NOT NULL,
    model_year integer NOT NULL,
    division_name citext NOT NULL,
    subdivision_name citext NOT NULL,
    model_name citext NOT NULL,
    style_name citext NOT NULL,
    trim_name citext,
    mfr_style_code citext,
    fleet_only citext NOT NULL,
    available_in_nvd citext NOT NULL,
    division_id integer NOT NULL,
    subdivision_id integer NOT NULL,
    model_id integer NULL,
    auto_builder_style_id citext,
    historical_style_id citext)
WITH (OIDS=FALSE);

create TABLE if not exists chr.Category(
    category_id integer primary key,
    description citext NOT NULL,
    category_utf citext,
    category_type citext)
WITH (OIDS=FALSE);

 
create TABLE if not exists chr.vin_pattern(
    vin_pattern_id integer primary key,
    vin_pattern citext NOT NULL,
    country citext NOT NULL,
    model_year integer NOT NULL,
    vin_division_name citext,
    vin_model_name citext NOT NULL,
    vin_style_name citext,
    engine_type_category_id integer,
    engine_size citext,
    engine_cid integer,
    fuel_type_category_id integer,
    forced_induction_category_id integer,
    transmission_type_category_id integer,
    manual_trnas_avail citext NOT NULL,
    auto_trans_avail citext NOT NULL,
    gvwr_range citext)
WITH (OIDS=FALSE);
CREATE INDEX vin_pattern_idx ON chr.vin_pattern(vin_pattern);
CREATE INDEX left_8_vin_pattern_idx ON chr.vin_pattern(left(vin_pattern, 8));

create TABLE if not exists chr.style_wheel_base(
    chrome_style_id integer not null references chr.year_make_model_style,
    wheel_base numeric(12,2),
    primary key (chrome_style_id, wheel_base))
WITH (OIDS=FALSE);
CREATE INDEX style_wheel_base_chrome_style_id_idx ON chr.style_wheel_base(chrome_style_id);

create TABLE if not exists chr.vin_pattern_style_mapping(
    vin_mapping_id integer primary key,
    chrome_style_id integer not null references chr.year_make_model_style,
    vin_pattern_id integer not null references chr.vin_pattern)
WITH (OIDS=FALSE);
CREATE INDEX vin_pattern_style_mapping_chrome_style_id_idx ON chr.vin_pattern_style_mapping(chrome_style_id);
CREATE INDEX vin_pattern_style_mapping_vin_pattern_id_idx ON chr.vin_pattern_style_mapping(vin_pattern_id);

create TABLE if not exists chr.style_generic_equipment(
    chrome_style_id integer not null references chr.year_make_model_style,
    category_id integer not null references chr.category,
    style_availability citext not null,
    primary key (chrome_style_id, category_id))
WITH (OIDS=FALSE);
CREATE INDEX style_generic_equipment_chrome_style_id_idx ON chr.style_generic_equipment(chrome_style_id);
CREATE INDEX style_generic_equipment_category_id_idx ON chr.style_generic_equipment(category_id);

 create TABLE if not exists chr.vin_equipment(
    vin_pattern_id integer not null references chr.vin_pattern,
    category_id integer not null references chr.category,
    vin_availability citext not null,
    primary key (vin_pattern_id, category_id))
 WITH (OIDS=FALSE);
 CREATE INDEX vin_equipment_vin_pattern_id_idx ON chr.vin_equipment(vin_pattern_id);
 CREATE INDEX vin_equipment_category_id_idx ON chr.vin_equipment(category_id);

create table if not exists chr.maint_vindata(
    table_name citext not null,
    file_name citext not null,
    load_sequence integer not null,
    truncate_sequence integer not null)
with (OIDS=FALSE);    


    