﻿select * 
from ads.ext_dpsvseries_vehicleitems a
left join chr.vin_pattern b on left(a.vin, 8) = left(vin_pattern, 8)
  and a.vin like replace(vin_pattern, '*', '_')
left join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
left join chr.year_make_model_style d on c.chrome_style_id = d.chrome_style_id
where length(a.vin) = 17
  and left(a.vin, 1) <> '0'
limit 100  
 


select a.*
-- select count(*) -- 244
from ads.ext_dpsvseries_vehicleitems a
left join chr.vin_pattern b on left(a.vin, 8) = left(vin_pattern, 8)
  and a.vin like replace(vin_pattern, '*', '_')
where length(a.vin) = 17
  and left(a.vin, 1) <> '0'
  and b.vin_pattern is null 

/*
vins:        45676
0 chrome id:   244
1 chrome id: 22935
2 chrome id:  7178
3 chrome id:  4902
4 chrome id:  3883
5 chrome id:  1373
6 chrome id:  2044
7 chrome id:   344
8 chrome id:   935
9 chrome id:   221
10 chrome id:  603
11 chrome id:   34
12 chrome id:  696
13 chrome id:  148
14 chrome id:   27
15 chrome id:   87
*/

select vin, count(*) as chrome_style_per_vin
from (
  select a.vin, d.chrome_style_id
  from ads.ext_dpsvseries_vehicleitems a
  left join chr.vin_pattern b on left(a.vin, 8) = left(vin_pattern, 8)
    and a.vin like replace(vin_pattern, '*', '_')
  left join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
  left join chr.year_make_model_style d on c.chrome_style_id = d.chrome_style_id
  where length(a.vin) = 17
    and left(a.vin, 1) <> '0' 
  group by a.vin, d.chrome_style_id) x
group by vin having count(*) = 15
