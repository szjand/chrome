import zipfile
import psycopg2
import glob
import os
import ftplib
import uuid
import datetime
# import csv

# TODO  include exception handling
# TODO report on success/fail
# TODO test for new file, e.g. nvd_v3_2018.zip
# TODO version update - get rid of the hard coding
# TODO combine new and used 10/30 not going to do this
# TODO ? bring in dps tables vehicleItems, makeModelClassification, BlackBookResolver ?
# TODO ??? update a chrome resolver table in DPS ??
pg_con = ''
log_cur = ''
pg_cur_1 = ''
pg_cur_2 = ''
pg_cur_3 = ''
log_id = ''
the_date = ''
the_time = ''
start_time = ''
rows = ''
schema = 'chr'
download_file = 'vindata.zip'
# Connection information
server = 'ftp.chromedata.com'
username = 'u265491'
password = 'car491'
# Directory and matching information
directory_used = '/VINMatch_US/'
filematch_used = '*.zip'
try:
    log_id = uuid.uuid4().urn[9:]
    the_date = datetime.date.today().strftime("%m/%d/%Y")
    the_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    log_cur = pg_con.cursor()
    pg_cur_1 = pg_con.cursor()
    pg_cur_2 = pg_con.cursor()
    pg_cur_3 = pg_con.cursor()
    files = glob.glob('files\zip_downloads\*.*')
    for f in files:
        os.remove(f)
    # Establish the connection
    start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    sql = ("""
        insert into chr.chrome_log (log_id, the_date, the_time, action_taken,
            load_start_ts) VALUES( """ +
           "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "','" +
           "ftp'," + "'" + start_time + "');""""
    """)
    log_cur.execute(sql)
    pg_con.commit()
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    # Change to the proper directory
    ftp.cwd(directory_used)
    # Loop through matching files and download each one individually
    for filename in ftp.nlst(filematch_used):
        fhandle = open('files/zip_downloads/' + filename, 'wb')
        ftp.retrbinary('RETR ' + filename, fhandle.write)
        fhandle.close()
    ftp.quit()
    end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    sql = """
        update chr.chrome_log
        set load_end_ts = """ + "'" + end_time + "'" + """
        where log_id = """ + "'" + log_id + "'" + """
        and the_date = """ + "'" + the_date + "'" + """
        and load_start_ts = """ + "'" + start_time + "'" + """
        and action_taken = 'ftp';
    """
    log_cur.execute(sql)
    pg_con.commit()
    # delete all rows from chr.tmp_version
    pg_cur_1.execute("""truncate chr.tmp_version;""")
    # delete all txt files from files\working_files
    files = glob.glob('files\working_files\*.txt')
    for f in files:
        os.remove(f)
    # extract only the version file
    zipfile.ZipFile('files\zip_downloads\VINDATA.zip').extract('Version.txt', 'files\working_files')
    io = open('files\working_files\Version.txt', 'r')
    pg_cur_1.copy_expert("""copy chr.tmp_version from stdin with csv header quote '~' """, io)
    pg_con.commit()
    io.close()
    # returns true if new version file is more recent than current version
    pg_cur_1.execute("""
          select (select data_version from chr.tmp_version) > a.data_version
          from chr.version a
          where a.file_name = '""" + download_file + "'""""
    """)
    for is_new_version in pg_cur_1:
        if is_new_version[0]:
            # if version file is more recent than current version
            zipfile.ZipFile('files\zip_downloads\VINDATA.zip').extractall('files\working_files')
            # truncate vindata tables
            pg_cur_2.execute("""
              select table_name
              from chr.maint_vindata
              order by truncate_sequence;
            """)
            for rows in pg_cur_2:
                start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                sql = ("""
                    insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name, action_taken, """ +
                       """ load_start_ts) """ +
                       """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                       "'" + download_file + "','" + rows[0] + "', 'truncate table','" +
                       start_time + "');""""
                """)
                log_cur.execute(sql)
                pg_con.commit()
                # table_name = schema + '.' + rows[0]
                pg_cur_3.execute("truncate " + schema + '.' + rows[0] + " CASCADE;")
                pg_con.commit()
                end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                sql = """
                    update chr.chrome_log
                    set load_end_ts = """ + "'" + end_time + "'" + """
                    where log_id = """ + "'" + log_id + "'" + """
                    and the_date = """ + "'" + the_date + "'" + """
                    and load_start_ts = """ + "'" + start_time + "'" + """
                    and table_name = """ + "'" + rows[0] + "';""""
                """
                log_cur.execute(sql)
                pg_con.commit()
            # load new data into tables
            pg_cur_2.execute("""
              select file_name, table_name
              from chr.maint_vindata
              order by load_sequence;
            """)
            for rows in pg_cur_2:
                start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                sql = ("""
                    insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name, action_taken, """ +
                       """ load_start_ts) """ +
                       """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                       "'" + download_file + "','" + rows[1] + "', 'populate table','" +
                       start_time + "');""""
                """)
                log_cur.execute(sql)
                pg_con.commit()
                file_name = 'files\working_files\\' + rows[0]
                # this section was necessary to process a bad VINPattern file with dup PK
                # if file_name == 'files\working_files\VINPattern.txt':
                #     os.rename('files\working_files\VINPattern.txt', 'files\working_files\VINPattern_orig.txt')
                #     with open('files\working_files\VINPattern_orig.txt', 'r') as inp, open(file_name, 'wb') as f:
                #         for row in csv.reader(inp):
                #             if row[0] != '3463218' and row[0] != '3463219':
                #                 csv.writer(f).writerow(row)
                #     inp.close()
                #     f.close()
                # if file_name == 'files\working_files\VINPatternStyleMapping.txt':
                #     os.rename('files\working_files\VINPatternStyleMapping.txt',
                #               'files\working_files\VINPatternStyleMapping_orig.txt')
                #     with open('files\working_files\VINPatternStyleMapping_orig.txt',
                #               'r') as inp, open(file_name, 'wb') as f:
                #         for row in csv.reader(inp):
                #             if row[2] != '3463218' and row[2] != '3463219':
                #                 csv.writer(f).writerow(row)
                #     inp.close()
                #     f.close()
                # if file_name == 'files\working_files\VINEquipment.txt':
                #     os.rename('files\working_files\VINEquipment.txt',
                #               'files\working_files\VINEquipment_orig.txt')
                #     with open('files\working_files\VINEquipment_orig.txt',
                #               'r') as inp, open(file_name, 'wb') as f:
                #         for row in csv.reader(inp):
                #             if row[0] != '3463218' and row[0] != '3463219':
                #                 csv.writer(f).writerow(row)
                #     inp.close()
                #     f.close()
                io = open(file_name, 'r')
                # will/can not generate null values for empty strings,
                # would have to preprocess the file or update the table
                pg_cur_3.copy_expert("""copy """ + schema + """.""" + rows[1] +
                                     """ from stdin with csv header quote '~' encoding 'latin-1' """, io)
                pg_con.commit()
                io.close()
                end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                sql = """
                    update chr.chrome_log
                    set load_end_ts = """ + "'" + end_time + "'" + """
                    where log_id = """ + "'" + log_id + "'" + """
                    and the_date = """ + "'" + the_date + "'" + """
                    and load_start_ts = """ + "'" + start_time + "'" + """
                    and table_name = """ + "'" + rows[1] + "';""""
                """
                log_cur.execute(sql)
                pg_con.commit()
            sql = ("""
                update chr.version
                set product = x.product,
                    data_version = x.data_version,
                    data_release_id = x.data_release_id,
                    schema_name = x.schema_name,
                    schema_version = x.schema_version,
                    country = x.country,
                    version_language = x.version_language,
                    last_updated = '""" + the_date + "'" +
                   """ from (
                     select a.*, current_date as last_update
                     from chr.tmp_version a) x
                   where file_name = """ + "'" + download_file + "';""""
            """)
            pg_cur_2.execute(sql)
            pg_con.commit()
        else:
            sql = ("""
                insert into chr.chrome_log (log_id, the_date, the_time, file_name, action_taken) """ +
                   """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                   "'" + download_file + "', 'no action taken');" + """
            """)
            log_cur.execute(sql)
            pg_con.commit()
            sql = ("""
                update chr.version
                set last_updated = '""" + the_date + "'" +
                   """ where file_name = """ + "'" + download_file + "';""""
            """)
            pg_cur_2.execute(sql)
            pg_con.commit()
except Exception, error:
    pg_con.rollback()
    sql = ("""
        insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name, action_taken,
           load_start_ts) """ +
           """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
           "'" + download_file + "','" + rows[1] + "'," + "'Error: " + str(error) + "'," +
           "'" + start_time + "');""""
    """)
    pg_cur_2.execute(sql)
    pg_con.commit()
    # print str(error)
finally:
    pg_cur_1.close()
    pg_cur_2.close()
    pg_cur_3.close()
    log_cur.close()
    pg_con.close()
