import zipfile
import psycopg2
import glob
import os
import ftplib
import uuid
import datetime
# initially loaded tables with download 10/20/15
# TODO  include exception handling
# TODO report on success/fail
# TODO test for new file, e.g. nvd_v3_2018.zip
# TODO version update - get rid of the hard coding
# TODO combine new and used
# TODO ? bring in dps tables vehicleItems, makeModelClassification, BlackBookResolver ?
# TODO ??? update a chrome resolver table in DPS ??
# 11/15/15
# 2 nights in a row new car simply stopped, incomplete, no errors
# revise error handling to log at the beginning of a block, update at the end
pg_con = ''
log_cur = ''
pg_cur_1 = ''
pg_cur_2 = ''
pg_cur_3 = ''
log_id = ''
start_time = ''
the_date = ''
the_time = ''
schema = 'chr'
# Connection information
server = 'ftp.chromedata.com'
username = 'u265491'
password = 'car491'
# # Directory and matching information
directory_new = '/NVD_Fleet_US_EN/All/'
filematch_new = 'nvd_v3_*'
try:
    log_id = uuid.uuid4().urn[9:]
    the_date = datetime.date.today().strftime("%m/%d/%Y")
    the_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    log_cur = pg_con.cursor()
    pg_cur_1 = pg_con.cursor()
    pg_cur_2 = pg_con.cursor()
    pg_cur_3 = pg_con.cursor()
    files = glob.glob('files\zip_downloads\*.*')
    for f in files:
        os.remove(f)
    # Establish the connection
    start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    # Change to the proper directory
    ftp.cwd(directory_new)
    # Loop through matching files and download each one individually
    for filename in ftp.nlst(filematch_new):
        fhandle = open('files/zip_downloads/' + filename, 'wb')
        ftp.retrbinary('RETR ' + filename, fhandle.write)
        fhandle.close()
    ftp.quit()
    end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    sql = ("""
        insert into chr.chrome_log (log_id, the_date, the_time, action_taken,
            load_start_ts, load_end_ts) VALUES( """ +
           "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "','" +
           "ftp'," + "'" + start_time + "','" + end_time + "');""""

    """)
    log_cur.execute(sql)
    pg_con.commit()
    # lists the files alphabetically, which happens to be just what i want
    # directory must contain no subdirectories, files only
    for download_file in os.listdir('files\zip_downloads'):
        # delete all rows from chr.tmp_version
        pg_cur_1.execute("""truncate chr.tmp_version;""")
        pg_con.commit()
        # delete all txt files from files\working_files
        files = glob.glob('files\working_files\*.txt')
        for f in files:
            os.remove(f)
        # extract only the version file and check it
        zipfile.ZipFile('files\zip_downloads\\' + download_file).extract('Version.txt', 'files\working_files')
        io = open('files\working_files\Version.txt', 'r')
        pg_cur_1.copy_expert("""copy chr.tmp_version from stdin with csv header quote '~' """, io)
        pg_con.commit()
        io.close()
        pg_cur_1.execute("""
          select (select data_version from chr.tmp_version) > a.data_version
          from chr.version a
          where a.file_name = """ + "'" + download_file + "'")
        for is_new_version in pg_cur_1:
            if is_new_version[0]:
                files = glob.glob('files\working_files\*.*')
                for f in files:
                    os.remove(f)
                # todo for some reason this line copys VINDATA.zip to working_files
                zipfile.ZipFile('files\zip_downloads\\' + download_file).extractall('files\working_files')
                pg_cur_2.execute("""
                    select tmp_table_name
                    from chr.maint_nvd
                    where table_name <> 'version'
                    order by truncate_sequence;
                """)
                # truncate the tmp_tables
                for rows in pg_cur_2:
                    pg_cur_3.execute("truncate " + schema + '.' + rows[0] + " CASCADE;")
                    pg_con.commit()
                pg_cur_2.execute("""
                    select file_name, tmp_table_name
                    from chr.maint_nvd
                    where table_name not in ('jpgs','version')
                    order by load_sequence;
                """)
                # populate tmp_tables
                for rows in pg_cur_2:
                    start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                    sql = ("""
                        insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name,  """ +
                           """ action_taken, load_start_ts) """ +
                           """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                           "'" + download_file + "'," + "'" + rows[1] + "', 'populate tmp table','" +
                           start_time + "');""""
                    """)
                    log_cur.execute(sql)
                    pg_con.commit()
                    io = open('files\working_files\\' + rows[0], 'r')
                    pg_cur_3.copy_expert("""copy """ + schema + """.""" + rows[1] + """ from stdin with
                                         csv header quote '~' encoding 'latin-1' """, io)
                    pg_con.commit()
                    end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                    sql = """
                        update chr.chrome_log
                        set load_end_ts = """ + "'" + end_time + "'" + """
                        where log_id = """ + "'" + log_id + "'" + """
                        and the_date = """ + "'" + the_date + "'" + """
                        and load_start_ts = """ + "'" + start_time + "'" + """
                        and table_name = """ + "'" + rows[1] + "';""""
                    """
                    log_cur.execute(sql)
                    pg_con.commit()
                    io.close()
                # i chose to use * for inserts based on the notion that the structure of
                # tables and tmp_tables MUST be identical
                # has_style_id = false
                pg_cur_2.execute("""
                    select table_name, tmp_table_name, primary_key
                    from chr.maint_nvd
                    where has_style_id = false
                      and table_name <> 'version'
                    order by load_sequence;
                """)
                for rows in pg_cur_2:
                    # insert any new rows
                    start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                    sql = ("""
                        insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name, """ +
                           """ action_taken, load_start_ts) """ +
                           """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                           "'" + download_file + "'," + "'" + rows[0] + "', 'has_style_id = false, populate tables','" +
                           start_time + "');""""
                    """)
                    log_cur.execute(sql)
                    pg_con.commit()
                    sql = ("""
                        insert into chr.""" + rows[0] +
                           """ select * from chr.""" + rows[1] +
                           """ a where not exists (select 1 from chr.""" + rows[0] +
                           """ where """ + rows[2] +
                           """ = a.""" + rows[2] + """);
                    """)
                    pg_cur_3.execute(sql)
                    pg_con.commit()
                    end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                    sql = """
                        update chr.chrome_log
                        set load_end_ts = """ + "'" + end_time + "'" + """
                        where log_id = """ + "'" + log_id + "'" + """
                        and the_date = """ + "'" + the_date + "'" + """
                        and load_start_ts = """ + "'" + start_time + "'" + """
                        and table_name = """ + "'" + rows[0] + "';""""
                    """
                    log_cur.execute(sql)
                    pg_con.commit()
                # has_style_id = true
                pg_cur_2.execute("""
                    select table_name, tmp_table_name
                    from chr.maint_nvd
                    where has_style_id = true
                      and table_name not in ('version','jpgs')
                    order by truncate_sequence;
                """)
                # has_style_id = true, delete the style_id everywhere it exists
                for rows in pg_cur_2:
                    sql = ("""
                        delete from chr.""" + rows[0] +
                           """ where style_id in (select style_id from chr.tmp_styles);
                       """)
                    pg_cur_3.execute(sql)
                    pg_con.commit()
                # has_style_id = true, insert new rows
                pg_cur_2.execute("""
                    select table_name, tmp_table_name
                    from chr.maint_nvd
                    where has_style_id = true
                      and table_name not in ('version','jpgs')
                    order by load_sequence;
                """)
                for rows in pg_cur_2:
                    start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                    sql = ("""
                        insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name, """ +
                           """ action_taken, load_start_ts) """ +
                           """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                           "'" + download_file + "'," + "'" + rows[0] + "', 'has_style_id = true, populate tables','" +
                           start_time + "');""""
                    """)
                    log_cur.execute(sql)
                    pg_con.commit()
                    sql = ("""
                        insert into chr.""" + rows[0] +
                           """ select * from chr.""" + rows[1] + """;
                    """)
                    pg_cur_3.execute(sql)
                    pg_con.commit()
                    end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                    sql = """
                        update chr.chrome_log
                        set load_end_ts = """ + "'" + end_time + "'" + """
                        where log_id = """ + "'" + log_id + "'" + """
                        and the_date = """ + "'" + the_date + "'" + """
                        and load_start_ts = """ + "'" + start_time + "'" + """
                        and table_name = """ + "'" + rows[0] + "';""""
                    """
                    log_cur.execute(sql)
                    pg_con.commit()
                start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                sql = ("""
                    update chr.version
                    set product = x.product,
                        data_version = x.data_version,
                        data_release_id = x.data_release_id,
                        schema_name = x.schema_name,
                        schema_version = x.schema_version,
                        country = x.country,
                        version_language = x.version_language,
                        last_updated = '""" + the_date + "'" +
                       """ from (
                      select *
                      from chr.tmp_version) x
                    where file_name = """ + "'" + download_file + "';""""
                """)
                pg_cur_2.execute(sql)
                pg_con.commit()
                end_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                sql = ("""
                    insert into chr.chrome_log (log_id, the_date, the_time, file_name, table_name, action_taken, """ +
                       """ load_start_ts, load_end_ts) """ +
                       """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                       "'" + download_file + "'," + " 'version', 'update version table','" +
                       start_time + "','" + end_time + "');""""
                """)
                log_cur.execute(sql)
                pg_con.commit()
            else:
                sql = ("""
                    insert into chr.chrome_log (log_id, the_date, the_time, file_name, action_taken) """ +
                       """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
                       "'" + download_file + "', 'no action taken');" + """
                """)
                log_cur.execute(sql)
                pg_con.commit()
                sql = ("""
                    update chr.version
                    set last_updated = '""" + the_date + "'" +
                       """ where file_name = """ + "'" + download_file + "';""""
                """)
                pg_cur_2.execute(sql)
                pg_con.commit()
except Exception, error:
    pg_con.rollback()
    # sql = ("""
    #     insert into chr.chrome_log (log_id, action_taken) """ +
    #        """ VALUES( """ + "'" + log_id + "'," +
    #        "'Error: " + str(error) + "');" + """
    # """)
    sql = ("""
        insert into chr.chrome_log (log_id, the_date, the_time, action_taken,
           load_start_ts) """ +
           """ VALUES( """ + "'" + log_id + "'," + "'" + the_date + "'," + "'" + the_time + "'," +
           "'Error: " + str(error) + "'," +
           "'" + start_time + "');""""
    """)
    log_cur.execute(sql)
    pg_con.commit()
    # print str(error)
finally:
    pg_cur_1.close()
    pg_cur_2.close()
    pg_cur_3.close()
    pg_con.close()
