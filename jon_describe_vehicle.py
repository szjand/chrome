import db_cnx
import requests
"""
5/20/18: this worked loaded 2536 vins & describe-vehicle responses into chr.describe_vehicle
12/20/18
  this is a copy of describe_vehicle.py that inserts (and updates) the data in 
  jon.describe_vehicle
  using it to generate the "new" chrome data that includes colors and options
  new end point that includes all: https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
"""

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # sql = "select '3GCUKREC5JG496297'"
        # sql = """
        #     select vin
        #     from (
        #       select vin, count(r.options->'header'->>'$value') as factory_options
        #       from chr.describe_vehicle a
        #       left join jsonb_array_elements(a.response->'factoryOption') as r(options) on true
        #       group by vin) b
        #     where factory_options < 3
        #       and not exists (
        #         select 1
        #         from jon.describe_vehicle
        #         where vin = b.vin)
        #       and right(vin,1) in ( '4','2','8','9');
        # """
        sql = """
            select vin
            from chr.describe_vehicle a
            where not exists (
              select 1
              from jon.describe_vehicle
              where vin = a.vin);
        """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into jon.describe_vehicle(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)
        with pg_con.cursor() as chr_cur:
            sql = """
                update jon.describe_vehicle y
                set style_count = x.style_count
                from (
                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'style')
                    from jon.describe_vehicle
                    where style_count is null) a
                  group by vin) x
                where x.vin = y.vin;  
            """
            chr_cur.execute(sql)