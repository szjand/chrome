﻿
11/26/19
calling it the refactor_cdc project
these are 3 query files from when i was getting started in october

current cdc is not working, these queries and .../chrome/chrome_v2.py are my attempt 
to clean it up
triggered by new colors not getting picked up 
and the belief that this data is going to be an integral part of building the ordering app

run both chrome.py and chrome_v2.py and see if i can sort out where i am in this process
chrome.py ~ 1 minute
run configurations.sql
chrome_v2 started at 8:55
failed with
    styles_cur.execute("select jon.xfm_styles()")
    psycopg2.errors.AssertFailure: changed style
but manually, function passes,
rerun styles section with out the xfm function    
and now the functions fails
this is the old thing about doing a commit on a connection when there are 
multiple cursor actions

added a pg.con_commit in styles, reran it
now it failed with the assert, but populated jon.get_styles, so i can see what the issues are

select 'get_styles' as source, c.*
from (
  select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
  from jon.get_styles,
    jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c
where chrome_style_id in ('406435','406436','406438','406439')       
union
select 'styles', a.*
from jon.styles a
where chrome_style_id in ('406435','406436','406438','406439')   
order by chrome_style_id, source

4 styles have the style_name changed, removing *Ltd Avail*

and, as i suspected, these are not new  models, so in chr.styles they did not get changed:
select * 
from chr.styles
where chrome_style_id in ('406435','406436','406438','406439')  


-- 1st file

-- Function: jon.xfm_models()

-- DROP FUNCTION jon.xfm_models();

CREATE OR REPLACE FUNCTION jon.xfm_models()
  RETURNS void AS
$BODY$

  -- new rows
  insert into jon.models(model_id,model_year,division_id,model)
  select * 
  from (
    select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
    from jon.get_models_by_division,
      jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) c
  where not exists (
    select 1
    from jon.models
    where model_id = c.model_id);

  -- test for changed model
  do
  $$
  begin
  assert(
  select count(*)
  from (
    select c.model_id, md5(c::text) as hash
    from (
      select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
      from jon.get_models_by_division,
        jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) c) d 
  join (
    select model_id, md5(a::text) as hash
    from jon.models a) e on d.model_id = e.model_id
      and d.hash <> e.hash) = 0, 'changed model';
  end
  $$;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION jon.xfm_models()
  OWNER TO rydell;


  -- ok, i think this is a one off, that CC is chassis cab and was included in the beginning
  -- for 2019 on, but my old way of processing did not pick it up because of the 
  -- on conflict do nothing
select 'models', a.*
from jon.models a
where model_id in (31262,31264,32213,32222)
union 
select 'get_models', a.*
from (
  select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
  from jon.get_models_by_division,
    jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) a
where model_id in (31262,31264,32213,32222)    


select * from jon.get_models_by_division where models::text like '%31264%'

select * from jon.models where model_id = 31264

select * from chr.get_models_by_division where models::text like '%31264%'

select * from chr.get_models_by_division where models::text like '%CC%'

select * 
from chr.models
where model_id in (31262,31264,32213,32222)    

update jon.models
set model = model || ' CC'
where model_id in (31262,31264,32213,32222)    

select * from chr.models order by model


-- 2nd file

-- Function: jon.xfm_styles()

-- DROP FUNCTION jon.xfm_styles();

CREATE OR REPLACE FUNCTION jon.xfm_styles()
  RETURNS void AS
$BODY$
  -- new rows
  insert into jon.styles (chrome_style_id,model_id,style_name)
  select *
    from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c
  where not exists (
    select 1
    from jon.styles
    where chrome_style_id = c.chrome_style_id);   

  -- test for changed style
  do $$
  begin
  assert (
    select count(*)
    from (
      select c.chrome_style_id, md5(c::text) as hash
      from (
        select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
        from jon.get_styles,
          jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c) d
    join (
      select chrome_style_id, md5(a::text) as hash
      from jon.styles a) e on d.chrome_style_id = e.chrome_style_id
        and d.hash <> e.hash) = 0, 'changed style';
  end $$;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION jon.xfm_styles()
  OWNER TO rydell;

-- yikes 111 changes, of course non of which were previously detected
-- go ahead and fix them, not a big deal, not currently using style_name anywhere
create temp table changed_styles as
select d.chrome_style_id
from (
  select c.chrome_style_id, md5(c::text) as hash
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) c) d
join (
  select chrome_style_id, md5(a::text) as hash
  from jon.styles a) e on d.chrome_style_id = e.chrome_style_id
    and d.hash <> e.hash;

select * from changed_styles

select 'styles' as source, a.*
from jon.styles a
join changed_styles b on a.chrome_style_id = b.chrome_style_id
union
select 'get_styles', a.*
from (
  select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
  from jon.get_styles,
    jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) a
join changed_Styles b on a.chrome_style_id = b.chrome_style_id
order by chrome_style_id, model_id, source
    

select * 
from (
  select 'styles' as source, a.*
  from jon.styles a
  join changed_styles b on a.chrome_style_id = b.chrome_style_id
  union
  select 'get_styles', a.*
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) a
  join changed_Styles b on a.chrome_style_id = b.chrome_style_id
  order by chrome_style_id, model_id, source) c
left join chr.models d on c.model_id = d.model_id


-- just go ahead and update the whole enchilada
update chr.styles x
set style_name = y.style_name
from (
  select *
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) c) y
where x.chrome_style_id = y.chrome_style_id    

select * from jon.styles

-- 3rd file


/*
select chrome_style_id
-- select *
from jon.get_Describe_Vehicle_by_style_id
limit 10


create table jon.describe_vehicle_by_style_id (
  chrome_style_id citext primary key,
  response jsonb not null);

create table chr.describe_vehicle_by_style_id (
  chrome_style_id citext primary key,
  response jsonb not null);
  
insert into chr.describe_vehicle_by_style_id  
select *
from jon.get_Describe_Vehicle_by_style_id


select now()

select * from jon.styles
*/



CREATE OR REPLACE FUNCTION jon.xfm_describe_vehicle_by_style_id()
  RETURNS void AS
$BODY$
  -- new rows
  insert into jon.describe_vehicle_by_style_id (chrome_style_id,response)
  select *
  from jon.get_describe_vehicle_by_style_id a
  where not exists (
    select 1
    from jon.describe_vehicle_by_style_id
    where chrome_style_id = a.chrome_style_id);   

  -- test for changed style
--   do $$
--   begin
--   assert (
-- fuck me, 2485 rows where hash <>

select c.*, d.* 
from (
  select a.chrome_style_id, md5(a::text) as hash
  from jon.describe_vehicle_by_style_id a) c
join (
  select a.chrome_style_id, md5(a::text) as hash
  from jon.get_describe_vehicle_by_style_id a) d on c.chrome_style_id = d.chrome_style_id
    and c.hash <> d.hash
join chr.configurations e on c.chrome_Style_id = e.chrome_style_id    


select jsonb_pretty(response)
from jon.describe_vehicle_by_style_id
where chrome_style_id = '405296'

select jsonb_pretty(response)
from jon.get_describe_vehicle_by_style_id
where chrome_style_id = '405296'


-- well, at least this was down to 166, need to look at what the diffs are
-- exclude market_class, down to 122 rows
-- exlcude style_name, down to 87
-- exclude trim_level, down to 49|:  all but one are adding CC to 3500HD, one is nissan 370Z model_code change
drop table if exists changed_styles;
create temp table changed_styles as
select aa.chrome_style_id
--   select *
from (
  select chrome_style_id, md5(tmp_configurations::text) as hash
  from tmp_configurations) aa
join (
  select chrome_style_id, md5(a::text) as hash
  from tmp_jon_configurations a) bb on aa.chrome_style_id = bb.chrome_style_id 
    and aa.hash <> bb.hash;
    
select * from changed_Styles    

select 'chrome' as source, a.*
from tmp_configurations a
join changed_styles b on a.chrome_style_id = b.chrome_style_id
union
select 'changed', a.*
from tmp_jon_configurations a
join changed_styles b on a.chrome_style_id = b.chrome_style_id
order by chrome_style_id, source


/*
-- exclude market class
-- leave out style_name
-- exclude trim_level
drop table if exists tmp_configurations;
create temp table tmp_configurations as
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, -- aa.trim_level, 
  coalesce(aa.peg, 'n/a') as peg, --aa.style_name, 
  aa.subdivision-- , aa.marketclass
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false') aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code;


drop table if exists tmp_jon_configurations;
create temp table tmp_jon_configurations as
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, -- aa.trim_level, 
  coalesce(aa.peg, 'n/a') as peg, --aa.style_name, 
  aa.subdivision-- , aa.marketclass
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass
  from jon.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false') aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code;        
*/   