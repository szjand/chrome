﻿select *
from chr.models
where model_id = 28578

select *
from chr.styles
where style_id = 378248

select *
from chr.options
where style_id = 378248

select * from chr.opt_headers where header_id = 1292  -- PREFERRED EQUIPMENT GROUP

select * from chr.opt_kinds where option_kind_id = 16 -- PEG


select a.availability, a.model_year, a.full_style_code, a.style_name, a.trim_level,
  a.cf_model_name, a.cf_style_name, a.cf_body_type, aa.option_code, a.front_wd, a.rear_wd, a.four_wd
from chr.styles a
join chr.options aa on a.style_id = aa.style_id
join chr.opt_kinds b on aa.option_kind_id = b.option_kind_id
  and b.option_kind = 'PEG'
join chr.opt_headers c on aa.header_id = c.header_id
  and c.opt_header = 'PREFERRED EQUIPMENT GROUP'
where a.model_year = 2017
  and a.availability not in ('F','P') -- excl fleet only and police only


-- colors
select *
from chr.styles a
where a.model_year = 2017
  and a.availability not in ('F','P') -- excl fleet only and police only

select *
from chr.manufacturers

drop table if exists makes_models;
create temp table makes_models as
select distinct a.division_name as make, b.model_name as model
from chr.divisions a
join chr.models b on a.division_id = b.division_id;
alter table makes_models
add primary key (make, model);

select b.model_year, a.division_name as make, b.model_name
from chr.divisions a
join chr.models b on a.division_id = b.division_id


