﻿figure out what i was looking at with engines

why the diff between engines and factory_options engines

these queries are all from ext_chrome/scripts/chrome_scratchpad_201905.sql

honda nissan does not include engine in factory option, there is no oemcode for the engine
-- engines

select e.chrome_style_id, 
  f.engines -> 'fuelType' ->> '$value' as fuel,
  f.engines -> 'engineType' ->> '$value' as engine_type,
  f.engines ->> 'cylinders' as cylinders,
  f.engines -> 'displacement' -> 'value' -> 0->> '$value' as displacement,
  f.engines -> 'displacement' -> 'value' -> 0 -> 'attributes' ->> 'unit' as units
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'engine') as f(engines) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful';


select chrome_style_id, option_code, replace(description, 'ENGINE, ', '') as engine
-- select *
from (
  select e.chrome_style_id, 
    f.factory_option -> 'header' -> 'attributes' ->> 'id' as header_id,
    f.factory_option -> 'header' ->> '$value' as the_header,
    f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
    f.factory_option -> 'description' ->> 0 as description
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'factoryOption') as f(factory_option) on true) x
  where the_header in ('ENGINE');


select chrome_style_id, count(*) from (
select a.chrome_style_id, 
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
  replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '') as description,
  case
    when position('L' in replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')) = 0
      then replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')
    else replace(
      substring(
        replace(
          replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL',''),
      position('L' in replace(
        replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL','')) -3, 4), 'L', '')
  end as displacement      
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and r.style ->'attributes'->>'modelYear' in ('2018','2019','2020')
  and r.style ->'division'->>'$value' in ('Buick','Chevrolet','GMC','Cadillac')
left join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
  and f.factory_option -> 'header' ->> '$value' = 'ENGINE'
order by chrome_Style_id  
) x group by chrome_style_id having count(*) > 2 order by count(*) desc     


gm engines: 
  left join jsonb_array_elements(e.response->'factoryOption') as h(gm_engines) on true
    and h.gm_engines -> 'header' ->> '$value' = 'ENGINE'
    and f.style -> 'division' ->> '$value' in ('Chevrolet','Buick','GMC','Cadillac')
    
-- gm vehicle with 4 engines
essentially, factory_options gives me the oemCode
do not initially see a clear way to correlate the engines array with the factoryOption

select jsonb_pretty(response) from chr.get_describe_vehicle_by_style_id where chrome_style_id = '401469'


assuming, and here testing, honda nissan will have a single engine options per model code
--hn engines
  left join jsonb_array_elements(e.response -> 'engine') as i(hn_engines) on true
    and f.style -> 'division' ->> '$value' in ('Honda','Nissan')

-- and we are good
select chrome_style_id from (
select a.chrome_Style_id, 
  r.style -> 'attributes' ->> 'model_year' as model_year,
  r.style -> 'division' ->> '$value' as make, 
  r.style -> 'model' ->> '$value' as model,
  i.hn_engines -> 'displacement' -> 'value' -> 0 ->> '$value' as engine
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and r.style ->'attributes'->>'modelYear' in ('2018','2019','2020')
  and r.style ->'division'->>'$value' in ('Honda','Nissan')
left join jsonb_array_elements(a.response -> 'engine') as i(hn_engines) on true
) x group by chrome_style_id 
having count(*) > 1


-- gm engines, lets look at factory vs engines
-- fuck it, way too much noise, wait for a need, for now, i can extract a description and a displacement & oemCode
select a.chrome_Style_id, 
  r.style -> 'attributes' ->> 'model_year' as model_year,
  r.style -> 'division' ->> '$value' as make, 
  r.style -> 'model' ->> '$value' as model,
  g.engines,
  h.gm_engines
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and r.style ->'attributes'->>'modelYear' in ('2018','2019','2020')
  and r.style ->'division'->>'$value' not in ('Honda','Nissan')
left join jsonb_array_elements(a.response -> 'engine') as g(engines) on true
left join jsonb_array_elements(a.response->'factoryOption') as h(gm_engines) on true
  and h.gm_engines -> 'header' ->> '$value' = 'ENGINE'
